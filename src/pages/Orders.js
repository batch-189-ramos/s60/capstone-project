import {useState, useEffect, useContext} from 'react';
import {useParams, Navigate } from 'react-router-dom';
import AdminOrder from '../components/AdminOrder';
import UserOrder from '../components/UserOrder';
import UserContext from '../UserContext';


export default function OrderView () {

	const {user} = useContext(UserContext);
	const {userId} = useParams();
	console.log(`${userId}`)
	const token = localStorage.getItem("token")

	const [orders, setOrders] = useState([])
	

	const getOrders = () => {
		fetch(`https://serene-gorge-68040.herokuapp.com/users/${userId}/getOrders`, {
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// console.log(`${userId}`)
			
			setOrders(data)
		})
	}

	useEffect(() => {
		getOrders()
	}, [])

	return ( user.isAdmin?
				<AdminOrder ordersProp={orders} getOrders={getOrders}/>
				:
				<UserOrder ordersProp={orders}/>

		)
}