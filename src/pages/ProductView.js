import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);

	// Allow us to gain access to methods that will allow us to redirect a user to a different page after ordering a product
	const navigate = useNavigate();// useHistory() - old versions
	// The "useParams"
	const {productId} =useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const order = (productId, productName, productPrice, quantity) => {

		fetch('https://serene-gorge-68040.herokuapp.com/users/order', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				Swal.fire({
					title: "Order Placed Successfully",
					icon: "success",
					text: "Your order will now be processed."
				})
				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {
		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [productId])

	return (

		<Container>
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card >
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					       
					       	{
					       		user.id !== null ?

					       		 <Button variant="primary" onClick={() =>  order(productId, name, price) }>Place Order</Button>
					       		 :

					       		 <Link className="btn btn-danger" to="/login"> Log in to add this to cart.
					       		 </Link>
					       	}
					      </Card.Body>
					</Card>
				</Col>
				
			</Row>

		</Container>


		)
}