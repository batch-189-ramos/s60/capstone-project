import {useState, useEffect, useContext} from 'react';
import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CreateProduct() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State Hooks to store the values of input fields of products
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [isActive, setIsActive] = useState(false)

	function createProduct(e) {
		e.preventDefault()

		fetch("https://serene-gorge-68040.herokuapp.com/products/checkProduct", {
			method: "POST",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				name: name
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data == true) {
				Swal.fire({
					title: "Duplicate Product Found",
					icon: "Error",
					text: "Kindly provide another name to complete product creation."
				})
			} else {
				fetch("https://serene-gorge-68040.herokuapp.com/products/createProducts", {
					method: "POST",
					headers: {
						'Content-Type': "application/json",
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						stocks: stocks
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data == true) {
						Swal.fire({
							title: "Product created successfully!",
							icon: "success",
							text: "You may now review the Products Catalog."
						})

						setName("");
						setDescription("");
						setPrice(0);
						setStocks(0);

						navigate("/adminDashboard")
					} else {
						Swal.fire({
							title: "Something went wrong.",
							icon: "error",
							text: "Please try again!"
						})
					}
				})
			}
		})

	}

	useEffect(() => {
		if(name !== '' && description !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price, stocks])


	return (

		<Container >
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Form className="mt-3" onSubmit={(e) => createProduct(e)} >
						<h3 className="mb-3">Add New Product Form</h3>
					      <Form.Group className="mb-3" controlId="productName">
					        <Form.Label>Name:</Form.Label>
					        <Form.Control type="text" placeholder="Enter Product Name" value={name} onChange={e =>setName(e.target.value)} required />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="description">
					        <Form.Label>Description:</Form.Label>
					        <Form.Control type="text" placeholder="Enter product details" value={description} onChange={e => setDescription(e.target.value)} required/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="price">
					        <Form.Label>Price:</Form.Label>
					        <Form.Control type="number" placeholder="Input product price" value={price} onChange={e => setPrice(e.target.value)} required/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="stocks">
					        <Form.Label>Stocks:</Form.Label>
					        <Form.Control type="number" placeholder="Input product stock" value={stocks} onChange={e => setStocks(e.target.value)} required/>
					      </Form.Group>

					      {
					      	isActive ?
					      		<Button variant="primary" type="submit">Submit
					      		</Button>
					      		:
					      		<Button variant="primary" type="submit" disabled>Submit
					      		</Button>
					      }
					      
					</Form>
				</Col>
			</Row>
		</Container>
		)
}