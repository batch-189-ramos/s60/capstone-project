import {Fragment, useEffect, useState, useContext} from 'react';
import productsData from '../data/productsData';
// import ProductCard from '../components/ProductCard';
import AdminDash from '../components/AdminDash';
import UserDash from '../components/UserDash';
import UserContext from '../UserContext';




export default function Products() {


	const { user } = useContext(UserContext);


	const [products, setProducts] = useState([]);

	const fetchData = () => {
		fetch('https://serene-gorge-68040.herokuapp.com/products/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data)
		})
	}

	useEffect(()=> {
		fetchData();
	}, [])

	
	return( user.isAdmin ?
			<AdminDash productsProp={products} fetchData={fetchData}/>
			:
			<UserDash productsProp={products}/>)

}