import { useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';



export default function UserOrder ({ordersProp}) {
	const [ordersArr, setOrdersArr] = useState([]);
	const orders = (orderProp) => {
		const {productId, _id} = orderProp
	}

	useEffect(() => {
		const orders = ordersProp.map(order => {
			if(order._id !== "undefined") {
				return (
					<>
						<Table>
							<tr key={order._id} orderProp={order}>
								<td>{order.productId}</td>
							</tr>
						</Table>
					</>
					)
			} else {
				return null
			}
		})
		setOrdersArr(orders)
	}, [ordersProp])


	return (

			<>

				<h3 className="my-4 text-center" >My Orders</h3>
				<Table>
					<thead>
						<tr>
							<th>Product Id</th>
						</tr>
					</thead>
					<tbody>
						{ordersArr}
					</tbody>
				</Table>

				
			</>

		)
}