import {useContext, useState, useEffect} from 'react';
// import CreateProduct from './CreateProduct';
import {Button, Row, Col, Container, Table, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
// import {Link} from 'react-router-dom';
// import ProductAdminView from '../components/ProductAdminView';
// import UserContext from '../UserContext';

export default function AdminDash(props) {

	// const {user} = useContext(UserContext);
	 const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([]);
	const [productId, setProductId] =useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [showAdd, setShowAdd] = useState(false);
	const [showUpdate, setShowUpdate] = useState(false);
	const token = localStorage.getItem("token")

	const openAdd = () => setShowAdd(true);	
	const closeAdd = () => setShowAdd(false);	

	const openUpdate = (productId) => {
		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStocks(data.stocks)

		})
		setShowUpdate(true)
	}

	const closeUpdate = () => {
		setName("");
		setDescription("");
		setPrice(0);
		setStocks(0);

		setShowUpdate(false);

	}



	const addProduct = (e) => {
		e.preventDefault();

		fetch("https://serene-gorge-68040.herokuapp.com/products/createProducts", {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
					Swal.fire({
						title: "Success!",
						icon: "success",
						text: "Product successfully added!"
					})
					fetchData()
					closeAdd()

					setName("");
					setDescription("");
					setPrice(0);
					setStocks(0);
			} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
					fetchData();
			}
		})
	}

	const updateProduct = (e) => {
		e.preventDefault();

		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
					Swal.fire({
						title: "Success!",
						icon: "success",
						text: "Product details successfully updated!"
					})
					fetchData()
					closeUpdate()

					setName("");
					setDescription("");
					setPrice(0);
					setStocks(0);
			} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
					fetchData();
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
					let bool

					isActive?
						bool = "disabled"
						:
						bool ="enabled"

					fetchData()

					Swal.fire({
						title: "Success!",
						icon: "success",
						text: `Product successfully ${bool}`
					})
			} else{
					fetchData()

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
			}
		})
	}

	useEffect(() => {
		const products = productsProp.map(product => {
			return (
					<tr key={product.id}>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>
							{product.isActive
								? <span>Avalable</span>
								: <span>Unavaliable</span>
							}
						</td>
						<td>
							<Button variant="primary" size="sm" onClick={() => openUpdate(product._id)}>Update</Button>
							{product.isActive
								? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
								: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
							}
						</td>

					</tr>

				)
		})
		setProductsArr(products)
	}, [productsProp])


	return (

			<>
				<div className="text-center my-4">
					<h2 className="my-4" >Admin DashBoard</h2>
					<h4 className="my-3" >Welcome Back Admin!</h4>
					<Button variant="dark" onClick={openAdd}>Add New Product</Button>
				</div>
			{/*Product Info Table*/}
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stocks</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productsArr}
				</tbody>

			</Table>


		{/*Add Product Modal*/}

		<Modal show={showAdd} onHide={closeAdd}>
			<Form onSubmit={e => addProduct(e)}>
				<Modal.Header closeButton>
										<Modal.Title>Add Product</Modal.Title>
									</Modal.Header>

									<Modal.Body>
										<Form.Group controlId="productName">
											<Form.Label>Name</Form.Label>
											<Form.Control
												value={name}
												onChange={e => setName(e.target.value)}
												type="text"
												required
											/>
										</Form.Group>

										<Form.Group controlId="productDescription">
											<Form.Label>Description</Form.Label>
											<Form.Control
												value={description}
												onChange={e => setDescription(e.target.value)}
												type="text"
												required
											/>
										</Form.Group>

										<Form.Group controlId="productPrice">
											<Form.Label>Price</Form.Label>
											<Form.Control
												value={price}
												onChange={e => setPrice(e.target.value)}
												type="number"
												required
											/>
										</Form.Group>

										<Form.Group controlId="productStocks">
											<Form.Label>Stocks</Form.Label>
											<Form.Control
												value={stocks}
												onChange={e => setStocks(e.target.value)}
												type="number"
												required
											/>
										</Form.Group>
									</Modal.Body>

									<Modal.Footer>
										<Button variant="secondary" onClick={closeAdd}>Close</Button>
										<Button variant="success" type="submit">Submit</Button>
									</Modal.Footer>

			</Form>
		</Modal>

	{/*Update Product Modal*/}
		<Modal show={showUpdate} onHide={closeUpdate}>
						<Form onSubmit={e => updateProduct(e)}>
							<Modal.Header closeButton>
								<Modal.Title>Update Product</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								<Form.Group controlId="productName">
									<Form.Label>Name</Form.Label>
									<Form.Control
										value={name}
										onChange={e => setName(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productDescription">
									<Form.Label>Description</Form.Label>
									<Form.Control
										value={description}
										onChange={e => setDescription(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productPrice">
									<Form.Label>Price</Form.Label>
									<Form.Control
										value={price}
										onChange={e => setPrice(e.target.value)}
										type="number"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productStocks">
									<Form.Label>Stocks</Form.Label>
									<Form.Control
										value={stocks}
										onChange={e => setStocks(e.target.value)}
										type="number"
										required
									/>
								</Form.Group>

							</Modal.Body>

							<Modal.Footer>
								<Button variant="secondary" onClick={closeUpdate}>Close</Button>
								<Button variant="success" type="submit">Submit</Button>
							</Modal.Footer>
						</Form>
		</Modal>




			</>
		)
	
}


/*return (

		
			<Container>
				<Row>
					<Col>

						<h2>Welcome back Admin!</h2>
						<Button className="mt-3" variant="dark" as={Link} to="/createProducts">Add a New Product</Button>
					</Col>
				</Row>

				<Row>
					<Col>
						<h1 className="mb-3 mt-3">Products</h1>
						{products}
					</Col>
				</Row>
			</Container>


		)*/