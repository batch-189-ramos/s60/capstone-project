
import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {

	return(

			<>
				<h3 className="my-4" >Customer's Feedback</h3>

				<Row className="mt-3 mb-3">
					<Col xs={12} md={4}>
					
						<Card className= "cardHighlight p-3">
						      <Card.Body>
						        <Card.Title>Naruto Uzumaki</Card.Title>
						        <Card.Subtitle className="mb-3">current Kazekage of Konoha</Card.Subtitle>
						        <Card.Text>
						          "The food selection was great and the prices are very affordable."
						        </Card.Text>
						      </Card.Body>
						</Card>
					</Col>

					<Col xs={12} md={4}>
						<Card className= "cardHighlight p-3">
						      <Card.Body>
						        <Card.Title>Kakashi Hatake</Card.Title>
						        <Card.Subtitle className="mb-3">from Konoha Academy</Card.Subtitle>
						        <Card.Text>
						          "For me, it's the Japchae that I like the most because of its savory taste."
						        </Card.Text>
						      </Card.Body>
						</Card>
					</Col>

					<Col xs={12} md={4}>
						<Card className= "cardHighlight p-3">
						      <Card.Body>
						        <Card.Title>Jiraiya</Card.Title>
						        <Card.Subtitle className="mb-3">One of Konoha's Legendary Sanin</Card.Subtitle>
						        <Card.Text>
						          "I highly recommend for everyone to try their Original Kimbap. It's an ideal snack specially for travellers."
						        </Card.Text>
						      </Card.Body>
						</Card>
					</Col>
				</Row>

			</>

		)
}