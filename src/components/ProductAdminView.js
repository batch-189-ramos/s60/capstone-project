import {useState, useEffect} from 'react';
import {Row, Col, Card, Button, Table} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductAdminView({productProp}) {

	
	function archiveProduct (productId, isActive) {
		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

	}


	function enableProduct (productId, isActive) {
		fetch(`https://serene-gorge-68040.herokuapp.com/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	}

	const { name, description, price, _id, stocks, isActive } = productProp;
	return (
		
			<Row>
				<Col>
					<Table striped>
					      <thead>
					        <tr>
					          <th>Product Name</th>
					          <th>Description</th>
					          <th>Price</th>
					          <th>Stocks</th>
					          <th>Availability</th>
					          <th>Actions</th>
					        </tr>
					      </thead>
					      <tbody>
					        <tr key={_id}>
					          <td>{name}</td>
					          <td>{description}</td>
					          <td>{price}</td>
					          <td>{stocks}</td>
					          <td>
					          		{isActive
					          			? <span>Available</span>
										: <span>Unavailable</span>
					          		}
					          </td>

					          <td>
					          		{isActive
					          			? <Button className="m-3" variant="danger" onClick={(e) => archiveProduct(_id, isActive)}>Archive</Button>
					          			: <Button className="m-3" variant="success" onClick={(e) => archiveProduct(_id, isActive) }>Enable</Button>
					          		}
					          </td>

					        </tr>
					      </tbody>
					    </Table>
					

				</Col>
			</Row>

		)

}


/* Another way of linking with buttons
<Link className="btn btn-primary" to="/productView">Details</Link>

*/

{/*<Card className= "cardHighlight p-3 m-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>  		
        <Card.Subtitle>Stocks:</Card.Subtitle>
        <Card.Text>{stocks}</Card.Text>      
        <Button className="m-3" variant="primary" as={Link} to={`/updateProducts/${_id}`}>Update</Button>
        <Button className="m-3" variant="danger" onClick={(e) => archiveProduct(_id, isActive) }>Archive</Button>
        <Button className="m-3" variant="warning" onClick={(e) => enableProduct(_id, isActive) }>Enable</Button>
      </Card.Body>
</Card>*/}