import { Row, Col, Button, Carousel} from 'react-bootstrap';

export default function Banner() {


	return(

			<>
				<Row>
					<Col className="p-5">	
						<h1 className="text-center"> Rup's Kitchen</h1>
						<h5 className="text-center" >Savour the taste of Korean dishes at the comfort of your home.</h5>

					{/*	<img
				           className="d-block w-100"
				           src="https://scontent.fmnl8-2.fna.fbcdn.net/v/t39.30808-6/273743822_119051207353756_2326516853709853391_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeEhQrEMDmjAKIj9vyv1kl7ScNhU0znaXkpw2FTTOdpeSsKh5alEX71kdWZtuKjCZNdWa0ibTTlZY0Qpv_M2d-of&_nc_ohc=XNnB4bsIiJ8AX-91Aqf&_nc_ht=scontent.fmnl8-2.fna&oh=00_AT8hhSwrVoxeEzo4XLF2rHx-9Y4QF_OlGUeYUizVRdJ0oQ&oe=62E1043F"
				           alt="First slide"
				           height="70%"
				         />
*/}
					</Col>
				 </Row>

				 <Carousel>
				       <Carousel.Item>
				         <img
				           className="d-block w-100"
				           src="https://scontent.fmnl8-1.fna.fbcdn.net/v/t39.30808-6/295459346_169105592348317_3330013284462660477_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGYFHY9aOa5jQrQjrDKq3o7ksI042RlakySwjTjZGVqTIuv5cft3XniV6q6XmyCP2xsboZpfx_MAljQEsfErf0k&_nc_ohc=RCsqYdrPG24AX-uYul5&tn=34vno2-boOs2wYyy&_nc_ht=scontent.fmnl8-1.fna&oh=00_AT8eb9r6Nhb2mUFPTsrQQSW8T3y3JfyrGyv7j-MVCAbGdg&oe=62E0FC24"
				           alt="First slide"
				         />
				         <Carousel.Caption>
				           <h3>Pork Bibimbap</h3>
				           <p>A bowl of rice topped with veggies and pork.</p>
				         </Carousel.Caption>
				       </Carousel.Item>
				       <Carousel.Item>
				         <img
				           className="d-block w-100"
				           src="https://scontent.fmnl8-1.fna.fbcdn.net/v/t39.30808-6/295476241_169105595681650_7470326000758806528_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeG8vkaKJ2U_8BQbggQj7o1OOELVAB4YUq84QtUAHhhSrwbMyIDL9FcFI45AiSnvGZEHtDAXUw2uJuIzhdtlvmXB&_nc_ohc=cbHhOFqEDTwAX92ENOO&tn=34vno2-boOs2wYyy&_nc_ht=scontent.fmnl8-1.fna&oh=00_AT_Mk1F85Nl1FZPnWW8EvXsgWtRrKwgWJaEnEf2UoYCE0w&oe=62E1817B"
				           alt="Second slide"
				         />

				         <Carousel.Caption>
				           <h3 className="" >Original Kimbap</h3>
				           <p>A favorite Korean sushi snack with various fillings.</p>
				         </Carousel.Caption>
				       </Carousel.Item>
				       <Carousel.Item>
				         <img
				           className="d-block w-100"
				           src="https://scontent.fmnl8-2.fna.fbcdn.net/v/t39.30808-6/295515329_169105635681646_3162580315791520476_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGirf1J8nea9Ne6hqzRD2ncCbS9yeSO9MQJtL3J5I70xEzbTKPHR_uLZ3su1q8ASP7e22RZXhDH-Gted-I-GSyS&_nc_ohc=K70gT2F3J80AX8d6hnl&_nc_ht=scontent.fmnl8-2.fna&oh=00_AT8dla2Q-li0vHemnQSh4MsVPz4oxUXN9frq46V1cmdSnQ&oe=62E29D56"
				           alt="Third slide"
				         />

				         <Carousel.Caption>
				           <h3>Beef Bulgogi</h3>
				           <p>
				             A popular savory dish made with beef, lots of onions and Bulgogi sauce.
				           </p>
				         </Carousel.Caption>
				       </Carousel.Item>
				     </Carousel>
			</>

		)


}
