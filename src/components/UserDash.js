import { useState, useEffect } from 'react';
// import productsData from './../data/productsData'
import ProductCard from './ProductCard'

export default function UserDash({productsProp}) {

	const [productsArr, setProductsArr] = useState([])

	// console.log(productData)

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		const products = productsProp.map(product => {
			// console.log(product)
			if(product.isActive){
				return <ProductCard key={product._id} productProp={product} />
			}else{
				return null
			}
		})

		setProductsArr(products)

	}, [productsProp])

	return(
		<>
			{productsArr}
		</>
	)
}
