import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
/*	console.log(props)
	console.log(typeof props)
	console.log(props.productProp.name)*/
	// console.log(productProp)

	/*
	useState - Use the state hook for this component to be able to store its state.
	States are used to keep track of information related to our individual components

		Syntax:
			const [getter, setter] = useState(initialGetterValue)	

	*/

	// // setCount is async
	// const [count, setCount] =useState(0);
	// const[seat, setSeat] =useState(30);

	// function enroll(enrollees, seats) {
		

	// 	// if (count < 30) {
	// 		setCount(count + 1)
	// 		console.log('Enrollees:' + count)
	// 		setSeat(seat -1)
	// 		console.log('Seats:' + seat)
	// 	// } else {
	// 	// 	alert ('no more seats.')
	// 	// }
		
	// }


	// useEffect(() => {
	// 	if ( seat === 0) {
	// 		alert('No more seats available')
	// 	}
	// }, [seat])

	// Deconstruct the product properties into their own variable name
	
	const { name, description, price, _id } = productProp;
	return (
		
			<Row>
				<Col>
					<Col xs={12} md={4}>
					<Card className= "cardHighlight p-3 m-3">
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>      
					        <Button variant="primary" as={Link} to={`/products/${_id}`}>Details</Button>

					      </Card.Body>
					</Card>
				</Col>
				</Col>
			</Row>

		)

}


/* Another way of linking with buttons
<Link className="btn btn-primary" to="/productView">Details</Link>

*/