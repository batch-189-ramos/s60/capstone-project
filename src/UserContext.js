import React from 'react';

// createContext allows to create a context object to pass information to other components
// A context object - as the name states is a data type of an object that can be used to store information that can be shared to other components within that app.
// Context Object is a different approach to passing information between components and allows easier access by avoiding the user of prop-drilling
const UserContext = React.createContext() 


// The "Provider" component allows other components to consume/use the context object and supply necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;
