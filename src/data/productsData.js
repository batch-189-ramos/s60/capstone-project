
const coursesData = [

	{
		id: "62db8c00b3a5f7a11847366c",
		name: "Beef Bibimbap",
		description: "A bowl of rice topped with veggies and beef.",
		price: 180,
		stocks: 120,
		isActive : true
	},
	{
		id: "BBAP002",
		name: "Pork Bibimbap",
		description: "A bowl of rice topped with veggies and pork.",
		price: 150,
		stocks: 100,
		isActive : true
	},
	{
		id: "BBAP001",
		name: "Chicken Bibimbap",
		description: "A bowl of rice topped with veggies and chicken.",
		price: 135,
		stocks: 100,
		isActive : true
	},
	{
		id: "JAP001",
		name: "Japchae",
		description: "A savory noodle dish with mix veggies, meat and egg.",
		price: 150,
		stocks: 100,
		isActive : true
	},
	{
		id: "BULG001",
		name: "Beef Bulgogi",
		description: "A bowl of rice topped with beef bulgogi.",
		price: 160,
		stocks: 80,
		isActive : true
	},
	{
		id: "KBAP001",
		name: "Original Kimbap",
		description: "A favorite Korean sushi snack with various fillings.",
		price: 120,
		stocks: 60,
		isActive : true
	},
	{
		id: "KBAP002",
		name: "Samgyupsal Kimbap",
		description: "A favorite Korean sushi snack with various fillings.",
		price: 120,
		stocks: 60,
		isActive : true
	},
	{
		id: "KBAP003",
		name: "SPAM Kimbap",
		description: "A favorite Korean sushi snack with various fillings.",
		price: 120,
		stocks: 60,
		isActive : true
	},
	{
		id: "KIMFR002",
		name: "Kimchi Fried Rice with SPAM and Egg",
		description: "A bowl of kimchi fried rice topped with 2 slices of SPAM and egg.",
		price: 120,
		stocks: 60,
		isActive : true
	}
]

export default coursesData;