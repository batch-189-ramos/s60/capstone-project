// import {Fragment} from 'react';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights'

// import AdminDash from './pages/AdminDash';
// import CreateProduct from './pages/CreateProduct'
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from './pages/Orders';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
// import UpdateProduct from './pages/UpdateProduct';
import './App.css';
import {UserProvider} from './UserContext';



function App() {

  // State hook for the user state that's defined here for a global scope/ store user info and for validating if user is logged in or not.

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing the localstore upon logout of user.
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('https://serene-gorge-68040.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router> 
        <AppNavbar/>
        <Container>
        <Routes> 
          <Route path="/" element={<Home/>}/>
          <Route path="/products" element={<Products/>}/>
          <Route path="/products/:productId" element={<ProductView/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="users/:userId/getOrders" element={<Orders/>}/>
          {/*<Route path="/adminDashboard" element={<AdminDash/>}/>*/}

          <Route path="*" element={<Error/>}/>

        {/*Admin Dashboard*/}
          {/*<Route path="/createProducts" element={<CreateProduct/>}/>*/}
          {/*<Route path="/updateProducts/:productId" element={<UpdateProduct/>}/>*/}

        </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
